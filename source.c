#include <FuzzyIO.h>
#include <FuzzyRuleConsequent.h>
#include <FuzzyRule.h>
#include <FuzzyOutput.h>
#include <FuzzyInput.h>
#include <FuzzySet.h>
#include <FuzzyRuleAntecedent.h>
#include <FuzzyComposition.h>
#include <Fuzzy.h>

#include <Stepper.h> 

Fuzzy *fuzzy = new Fuzzy();
Fuzzy *fuzzyLED = new Fuzzy();
 
const int stepsPerRevolution = 500; 
int lightSensorPin = A5;
int lightSensorValue = 0;
int ledOutputPin = 3;
int ledOutputValue = 0;

const uint8_t trig_pin = 4;
const uint8_t echo_pin = 5;

uint32_t echo_timer = 0;

  
//Inicializa a biblioteca utilizando as portas de 8 a 11 para 
//ligacao ao motor 
Stepper myStepper(stepsPerRevolution, 8,10,9,11); 
  
void setup() 
{ 
  // Instantiating a FuzzyInput DISTANCE objects
  FuzzyInput *distance = new FuzzyInput(1);
  FuzzySet *small = new FuzzySet(0, 10, 10, 20);
  distance->addFuzzySet(small);
  FuzzySet *safe = new FuzzySet(15, 30, 30, 45);
  distance->addFuzzySet(safe);
  FuzzySet *big = new FuzzySet(40, 50, 50, 50);
  distance->addFuzzySet(big);
  fuzzy->addFuzzyInput(distance);
  
  // Instantiating a FuzzyOutput SPEED objects
  FuzzyOutput *speed = new FuzzyOutput(1);
  FuzzySet *slow = new FuzzySet(0, 10, 10, 20);
  speed->addFuzzySet(slow);
  FuzzySet *average = new FuzzySet(10, 20, 30, 40);
  speed->addFuzzySet(average);
  FuzzySet *fast = new FuzzySet(30, 40, 50, 60);
  speed->addFuzzySet(fast);
  fuzzy->addFuzzyOutput(speed);
  
   // Instantiating a FuzzyInput LUMINOSITY objects
  FuzzyInput *luminosity = new FuzzyInput(2);
  FuzzySet *bright = new FuzzySet(512, 768, 1023, 1023); 
  luminosity->addFuzzySet(bright);
  FuzzySet *mediumLight = new FuzzySet(256, 512, 512, 768);
  luminosity->addFuzzySet(mediumLight);
  FuzzySet *dark = new FuzzySet(0, 0, 256, 512);
  luminosity->addFuzzySet(dark);
  fuzzyLED->addFuzzyInput(luminosity);
  
   // Instantiating a FuzzyOutput LED-FREQUENCIE objects
  FuzzyOutput *ledFreq = new FuzzyOutput(2);
  FuzzySet *highFreq = new FuzzySet(150, 200, 255, 255);
  ledFreq->addFuzzySet(highFreq);
  FuzzySet *medFreq = new FuzzySet(50, 100, 150, 200);
  ledFreq->addFuzzySet(medFreq);
  FuzzySet *lowFreq = new FuzzySet(0, 50, 50, 100);
  ledFreq->addFuzzySet(lowFreq);
  fuzzyLED->addFuzzyOutput(ledFreq);


  // LED Consequents
  FuzzyRuleConsequent *thenFreqHigh = new FuzzyRuleConsequent();
  thenFreqHigh->addOutput(highFreq);
  
  FuzzyRuleConsequent *thenFreqMed = new FuzzyRuleConsequent();
  thenFreqMed->addOutput(medFreq);

  FuzzyRuleConsequent *thenFreqLow = new FuzzyRuleConsequent();
  thenFreqLow->addOutput(lowFreq);


  // SPEED Consequents
  FuzzyRuleConsequent *thenSpeedSlow = new FuzzyRuleConsequent();
  thenSpeedSlow->addOutput(slow);

  FuzzyRuleConsequent *thenSpeedAverage = new FuzzyRuleConsequent();
  thenSpeedAverage->addOutput(average);

  FuzzyRuleConsequent *thenSpeedFast = new FuzzyRuleConsequent();
  thenSpeedFast->addOutput(fast);

  // Antecedents with logic connector
  //IF lumi->dark and distance ->small 
  FuzzyRuleAntecedent *ifLumiDarkAndDistanceSmall = new FuzzyRuleAntecedent();
  ifLumiDarkAndDistanceSmall->joinWithAND(dark, small);
  
  //IF lumi->dark and distance -> safe
  FuzzyRuleAntecedent *ifLumiDarkAndDistanceSafe = new FuzzyRuleAntecedent();
  ifLumiDarkAndDistanceSafe->joinWithAND(dark, safe);

  //IF lumi->dark and distance -> big
  FuzzyRuleAntecedent *ifLumiDarkAndDistanceBig = new FuzzyRuleAntecedent();
  ifLumiDarkAndDistanceBig->joinWithAND(dark, big);
  
  
  //IF lumi->mediumLight and distance ->small 
  FuzzyRuleAntecedent *ifLumiMediumLightAndDistanceSmall = new FuzzyRuleAntecedent();
  ifLumiMediumLightAndDistanceSmall->joinWithAND(mediumLight, small);
  
  //IF lumi->mediumLight and distance -> safe
  FuzzyRuleAntecedent *ifLumiMediumLightAndDistanceSafe = new FuzzyRuleAntecedent();
  ifLumiMediumLightAndDistanceSafe->joinWithAND(mediumLight, safe);

  //IF lumi->mediumLight and distance -> big
  FuzzyRuleAntecedent *ifLumiMediumLightAndDistanceBig = new FuzzyRuleAntecedent();
  ifLumiMediumLightAndDistanceBig->joinWithAND(mediumLight, big);
  
  
  //IF lumi->bright and distance ->small 
  FuzzyRuleAntecedent *ifLumiBrightAndDistanceSmall = new FuzzyRuleAntecedent();
  ifLumiBrightAndDistanceSmall->joinWithAND(bright, small);
  
  //IF lumi->bright and distance -> safe
  FuzzyRuleAntecedent *ifLumiBrightAndDistanceSafe = new FuzzyRuleAntecedent();
  ifLumiBrightAndDistanceSafe->joinWithAND(bright, safe);

  //IF lumi->bright and distance -> big
  FuzzyRuleAntecedent *ifLumiBrightAndDistanceBig = new FuzzyRuleAntecedent();
  ifLumiBrightAndDistanceBig->joinWithAND(bright, big);
  
  //rule IF lumi->dark and distance->small then speed->fast and freq->low
  FuzzyRule* fuzzyRule01 = new FuzzyRule(1, ifLumiDarkAndDistanceSmall, thenSpeedFast);
  FuzzyRule* fuzzyRule02 = new FuzzyRule(2, ifLumiDarkAndDistanceSmall, thenFreqLow);

  //rule IF lumi->dark and distance->safe then speed->average and freq->low
  FuzzyRule* fuzzyRule03 = new FuzzyRule(3, ifLumiDarkAndDistanceSafe, thenSpeedAverage);
  FuzzyRule* fuzzyRule04 = new FuzzyRule(4, ifLumiDarkAndDistanceSafe, thenFreqLow);

  //rule IF lumi->dark and distance->big then speed->average and freq->medium
  FuzzyRule* fuzzyRule05 = new FuzzyRule(5, ifLumiDarkAndDistanceBig, thenSpeedAverage);
  FuzzyRule* fuzzyRule06 = new FuzzyRule(6, ifLumiDarkAndDistanceBig, thenFreqMed);

  //rule IF lumi->medium light and distance->small then speed->slow and freq->low
  FuzzyRule* fuzzyRule07 = new FuzzyRule(7, ifLumiMediumLightAndDistanceSmall, thenSpeedSlow);
  FuzzyRule* fuzzyRule08 = new FuzzyRule(8, ifLumiMediumLightAndDistanceSmall, thenFreqLow);
  
  //rule IF lumi->medium light and distance->safe then speed->fast and freq->high
  FuzzyRule* fuzzyRule09 = new FuzzyRule(9, ifLumiMediumLightAndDistanceSafe, thenSpeedFast);
  FuzzyRule* fuzzyRule10 = new FuzzyRule(10, ifLumiMediumLightAndDistanceSafe, thenFreqHigh);

  //rule IF lumi->medium light and distance->big then speed->average and freq->high
  FuzzyRule* fuzzyRule11 = new FuzzyRule(11, ifLumiMediumLightAndDistanceBig, thenSpeedAverage);
  FuzzyRule* fuzzyRule12 = new FuzzyRule(12, ifLumiMediumLightAndDistanceBig, thenFreqHigh);

  //rule IF lumi->bright and distance->small then speed->slow and freq->high
  FuzzyRule* fuzzyRule13 = new FuzzyRule(13, ifLumiBrightAndDistanceSmall, thenSpeedSlow);
  FuzzyRule* fuzzyRule14 = new FuzzyRule(14, ifLumiBrightAndDistanceSmall, thenFreqHigh);

  //rule IF lumi->bright and distance->safe then speed->fast and freq->high
  FuzzyRule* fuzzyRule15 = new FuzzyRule(15, ifLumiBrightAndDistanceSafe, thenSpeedFast);
  FuzzyRule* fuzzyRule16 = new FuzzyRule(16, ifLumiBrightAndDistanceSafe, thenFreqHigh);

  //rule IF lumi->bright and distance->big then speed->fast and freq->high
  FuzzyRule* fuzzyRule17 = new FuzzyRule(17, ifLumiBrightAndDistanceBig, thenSpeedFast);
  FuzzyRule* fuzzyRule18 = new FuzzyRule(18, ifLumiBrightAndDistanceBig, thenFreqHigh);

  fuzzy->addFuzzyRule(fuzzyRule01);
  fuzzyLED->addFuzzyRule(fuzzyRule02);
  
  fuzzy->addFuzzyRule(fuzzyRule03);
  fuzzyLED->addFuzzyRule(fuzzyRule04);  
  
  fuzzy->addFuzzyRule(fuzzyRule05);
  fuzzyLED->addFuzzyRule(fuzzyRule06);  
  
  fuzzy->addFuzzyRule(fuzzyRule07);
  fuzzyLED->addFuzzyRule(fuzzyRule08);  
  
  fuzzy->addFuzzyRule(fuzzyRule09);
  fuzzyLED->addFuzzyRule(fuzzyRule10);  
  
  fuzzy->addFuzzyRule(fuzzyRule11);
  fuzzyLED->addFuzzyRule(fuzzyRule12);  
  
  fuzzy->addFuzzyRule(fuzzyRule13);
  fuzzyLED->addFuzzyRule(fuzzyRule14);  
  
  fuzzy->addFuzzyRule(fuzzyRule15);
  fuzzyLED->addFuzzyRule(fuzzyRule16);  
  
  fuzzy->addFuzzyRule(fuzzyRule17);
  fuzzyLED->addFuzzyRule(fuzzyRule18);

  pinMode(trig_pin, OUTPUT);

  pinMode(echo_pin, INPUT);

  digitalWrite(trig_pin, LOW);
  pinMode(ledOutputPin, OUTPUT);  
  Serial.begin(9600);

   //Determina a velocidade inicial do motor 
   myStepper.setSpeed(0);
} 
  
void loop() 
{ 
 // lightSensorValue = analogRead(lightSensorPin);
 //Serial.println(lightSensorValue);

int speed = 0;
// Espera 0,5s (500ms) entre medições.

if (millis() - echo_timer > 500) {
  echo_timer = millis();

  // Pulso de 5V por pelo menos 10us para iniciar medição.
  digitalWrite(trig_pin, HIGH);
  delayMicroseconds(11);
  digitalWrite(trig_pin, LOW);

  /* Mede quanto tempo o pino de echo ficou no estado alto, ou seja,
  o tempo de propagação da onda. */ 
  uint32_t pulse_time = pulseIn(echo_pin, HIGH);

  
  /* A distância entre o sensor ultrassom e o objeto será proporcional a velocidade
  do som no meio e a metade do tempo de propagação. Para o ar na
  temperatura ambiente Vsom = 0,0343 cm/us. */
  int distance = 0.01715 * pulse_time;


  // Imprimimos o valor na porta serial;    
  Serial.print("Distancia:  ");
  Serial.println(distance);
  fuzzy->setInput(1, distance);
  Serial.println("fuzzifying...");
  fuzzy->fuzzify();
  Serial.println("defuzzifying...");
  speed = (int)fuzzy->defuzzify(1);
  
  Serial.print("Velocidade:  ");
  Serial.println(speed);
  myStepper.setSpeed(speed);
  myStepper.step(1*speed); 
  lightSensorValue = analogRead(lightSensorPin);
  
  Serial.println("********************");
  Serial.print("Luminosity: ");
  Serial.println(lightSensorValue);
  fuzzyLED->setInput(2, lightSensorValue);
  fuzzyLED->fuzzify();
  ledOutputValue = (int)fuzzyLED->defuzzify(2);
  Serial.print("Freq do led:: ");
  Serial.println(ledOutputValue);
  analogWrite(ledOutputPin, ledOutputValue);
}


}